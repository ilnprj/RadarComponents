﻿// MIT License
// https://gitlab.com/ilnprj 
// Copyright (c) 2020 ilnprj


using UnityEngine;

namespace RadarComponents
{
    /// <summary>
    /// Simple circle radar without items
    /// </summary>
    public class CircleRadar : AbstractRadar
    {
        private float calibrate = 0f;

        protected override void Start()
        {
            base.Start();
            if (locator!=null)
            calibrate = locator.transform.eulerAngles.y - 360;
            else
            {
                Invoke("Delay", 1.0f);
            }
        }

        private void Delay()
        {
            locator = FindObjectOfType<PlayerLocator>();
            calibrate = locator.transform.eulerAngles.y - 360;
        }

        public override void OnUpdateRadar()
        {
            if (locator!=null)
            transform.rotation = Quaternion.Euler(0, 0, (locator.transform.eulerAngles.y + calibrate));
        }
    }
}

